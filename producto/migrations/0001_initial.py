# Generated by Django 2.1.3 on 2019-05-03 04:14

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='producto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fotografiaProducto', models.ImageField(upload_to='')),
                ('codigoProducto', models.CharField(max_length=100)),
                ('nombreProducto', models.CharField(max_length=100)),
                ('precioProducto', models.BigIntegerField()),
                ('stockProducto', models.BigIntegerField()),
                ('stockCriticoProducto', models.BigIntegerField()),
                ('fechaVencimientoProducto', models.DateField()),
                ('descripcionProducto', models.TextField()),
            ],
        ),
    ]
