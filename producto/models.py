from django.db import models
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
#importando la clase clientes
from clientes.models import Cliente
from django.contrib.auth import get_user_model
# Create your models here.

class familia(models.Model):
    nombreFamilia = models.CharField(max_length = 100)
    descripcionFamilia = models.TextField()

    class Meta:
        verbose_name="Familia Producto"

    def __str__(self):
        return self.nombreFamilia

class categoria(models.Model):
    nombreCategoria = models.CharField(max_length = 150)
    descripcionCategoria = models.TextField()
    nomFamilia = models.ForeignKey(familia, null=False, blank=False, on_delete=models.CASCADE)

    class Meta:
        verbose_name="Categoría"

    def __str__(self):
        return self.nombreCategoria

class producto(models.Model):
    fotografiaProducto = models.ImageField(upload_to = "media") 
    codigoProducto = models.CharField(max_length = 100)
    nombreProducto = models.CharField(max_length = 100)
    precioProducto = models.BigIntegerField()
    stockProducto = models.BigIntegerField()
    stockCriticoProducto = models.BigIntegerField()
    fechaVencimientoProducto = models.DateField()
    descripcionProducto = models.TextField()
    nomCategoria = models.ForeignKey(categoria, null=False, blank=False, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombreProducto

class carritoCompras(models.Model):
    producto=models.ForeignKey(producto, verbose_name="producto",on_delete=models.CASCADE)
    usuario=models.ForeignKey(get_user_model(),verbose_name="cliente", on_delete=models.CASCADE)
    precio = models.IntegerField()
    cantidad=models.IntegerField()
    comprado = models.BooleanField(default = False)
    pendiente = models.BooleanField(default = False)

    def __str__(self):
        return "{} {}".format(self.usuario, self.producto)
