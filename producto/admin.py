from django.contrib import admin
from .models import producto, familia, categoria,carritoCompras

admin.site.register(producto)
admin.site.register(familia)
admin.site.register(categoria)
admin.site.register(carritoCompras)