from django.contrib import admin
from django.urls import path ,include
from producto import views
from producto.views import ListarProducto,DetalleProducto, AnadirCarrito


urlpatterns = [
    path('agregarProducto/', views.agregarProducto, name="agregarProducto"),
    path('lista_productos/',ListarProducto.as_view(),name='lista_producto'),
    path('<int:pk>/<slug:slug>',DetalleProducto.as_view(),name="detalle_producto"),
    path('anadir_carrito/', AnadirCarrito.as_view(), name='anadir_carrito'),
]
