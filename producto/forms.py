from django import forms

from .models import producto

class ProductosForm(forms.ModelForm):
    class Meta:
        model = producto
        fields = ('fotografiaProducto', 'codigoProducto', 'nombreProducto', 'precioProducto', 'stockProducto', 'stockCriticoProducto', 'fechaVencimientoProducto', 'descripcionProducto', 'nomCategoria')

