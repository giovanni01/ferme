from django.shortcuts import render
from .forms import ProductosForm
from .models import producto,categoria, carritoCompras
from django.urls import reverse
from django.urls import reverse_lazy, reverse
#importando las vistas genericas
from django.views.generic.list import ListView
from django.views.generic import DetailView, CreateView, RedirectView


# Create your views here.

def agregarProducto(request):
    if request.method == "POST":
        producto = ProductosForm(request.POST, request.FILES)
        if producto.is_valid():
            post = producto.save(commit=False)
            post.save()
    else:
        producto = ProductosForm()
    categorias = categoria.objects.order_by('nombreCategoria')
    return render(request,"producto/agregarProducto.html", {'producto': producto, 'categorias':categorias})


class DetalleProducto(DetailView):
    template_name="producto/detalle_producto.html"
    model=producto



class ListarProducto(ListView):
    template_name="producto/lista_producto.html"
    model=producto
    paginate_by=10
    #agregar funcion busqueda producto


class AnadirCarrito(CreateView):
    model = carritoCompras
    fields = ('producto', 'usuario', 'precio','cantidad',)
    success_url = reverse_lazy('lista_producto')
    login_url = 'login'
