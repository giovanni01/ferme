from django import forms
from django.contrib.auth.forms import UserCreationForm

from . models import Cliente


class ClienteForm(UserCreationForm):
    
    
    class Meta:
        model=Cliente
        fields=("rut","nombre","fecha_nac","username","email","password1","password2","tipo_cliente")
        widgets={
            #escribimoa el campo y le damos los atributos con attrs
            'fecha_nac':forms.DateInput(attrs={
            'class': 'datepicker'
            
        }),
            
        }