from django.db import models

# Create your models here.
from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class tipoCliente(models.Model):
    descripcion = models.CharField(verbose_name="tipo de cliente",max_length=200)

    class Meta:
        verbose_name = "Tipo cliente"
        verbose_name_plural = "Tipo cliente"
        

    def __str__(self):
        return self.descripcion
class Cliente(User):
    rut=models.CharField(verbose_name="rut cliente",max_length=20)
    nombre=models.CharField(verbose_name="nombre",max_length=200)
    fecha_nac=models.DateField(verbose_name="fecha de nacimiento")
    tipo_cliente=models.ForeignKey(tipoCliente , verbose_name="tipo de cliente",on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Cliente"
        verbose_name_plural = "Clientes"
        

    def __str__(self):
        return self.nombre