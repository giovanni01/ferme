# Generated by Django 2.1.3 on 2019-05-09 04:39

from django.conf import settings
import django.contrib.auth.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0009_alter_user_last_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cliente',
            fields=[
                ('user_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('rut', models.CharField(max_length=20, verbose_name='rut cliente')),
                ('nombre', models.CharField(max_length=200, verbose_name='nombre')),
                ('fecha_nac', models.DateTimeField(verbose_name='fecha de nacimiento')),
            ],
            options={
                'verbose_name': 'Cliente',
                'verbose_name_plural': 'Clientes',
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='tipoCliente',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(max_length=200, verbose_name='tipo de cliente')),
            ],
            options={
                'verbose_name': 'Tipo cliente',
                'verbose_name_plural': 'Tipo cliente',
            },
        ),
        migrations.AddField(
            model_name='cliente',
            name='tipo_cliente',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='clientes.tipoCliente', verbose_name='tipo de cliente'),
        ),
    ]
