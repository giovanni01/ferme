
from .forms import ClienteForm
from django.views.generic import CreateView
from django.urls import reverse_lazy 

# Create your views here.

class RegitroCliente(CreateView):
    form_class=ClienteForm
    
    template_name="registration/signup.html"
    def get_success_url(self):
        return reverse_lazy('login')

"""
def signup(request):
    form = UserCreationForm()
    return render(request,'registration/signup.html',{'form':form})
"""