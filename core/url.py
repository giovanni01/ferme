from django.urls import path
from core import views
urlpatterns = [
    path('', views.home ,name="home"),
    path('contacto/', views.contacto ,name="contacto"),
    path('quienes somos/', views.quienesSomos ,name="quienesSomos"),
    path('servicios/',views.servicios,name="servicios"),
   
]
